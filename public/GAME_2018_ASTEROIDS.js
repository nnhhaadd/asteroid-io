function setup() {
  createCanvas(windowWidth, windowHeight);
  ship = new Ship();
  socket= io.connect('http://localhost:8080');
}
function draw() {
  background(0);
  mouse = createVector(mouseX, mouseY);
  ship.render();
  send();
}