
function Ship()
{
  this.pos = createVector(width/2, height/2);
  this.r  = 30;
  this.render = function()
  {
    this.dir = p5.Vector.sub(this.pos, mouse);
    this.heading =this.dir.heading()-PI/2;  // send
    translate(this.pos.x, this.pos.y);
    rotate(this.heading);
    noFill();
    strokeWeight(5);
    stroke(0, 255, 0);
    triangle(-this.r, this.r, this.r, this.r, 0, -this.r);
    rotate(-this.heading);
    translate(-this.pos.x, -this.pos.y);
  }
  this.turn = function(angle)
  {
    this.heading += angle;
  }
}