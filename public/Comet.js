function Comet(x, y, r)
{
  this.r = r;
  this.pos = createVector(x, y);
  this.heading = createVector(random(width), random(height));
  this.heading.sub(this.pos);

  this.render = function()
  {
    push();
    this.heading.normalize();
    this.vel  = random(2, 10);
    this.heading.mult(this.vel);
    stroke(255);
    strokeWeight(5);
    ellipse(this.pos.x, this.pos.y, this.r);
    pop();
  }

  this.turn = function(angle)
  {
    this.heading += angle;
  }
  this.update = function()
  {
    this.pos.add(this.heading);
    if (this.pos.y+this.r/2>height||this.pos.y-this.r/2<0)
    {
      this.heading.y*=-1;
    }
    if (this.pos.x+this.r/2>width ||this.pos.x-this.r/2<0)
    {
      this.heading.x*=-1;
    }
  }
  
}