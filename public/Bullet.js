function bullet(spos, heading)
{
  this.pos = createVector(spos.x, spos.y);
  this.heading = createVector(heading.x, heading.y);
  this.heading.normalize();
  this.vel  = 20;
  this.heading.mult(-this.vel);
  this.r = 15;
  this.render = function()
  {
    push();
    stroke(255);
    strokeWeight(this.r);
    point(this.pos.x, this.pos.y);
    pop();
  }
  this.turn = function(angle)
  {
    this.heading += angle;
  }
  this.update = function()
  {
    this.pos.add(this.heading);
  }
}