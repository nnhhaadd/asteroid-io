const express = require('express');

const app = express();

let server = require('http').Server(app);

let io = require('socket.io')(server);

const port = process.env.PORT || 8080;
const ip = process.env.IP || '0.0.0.0';

pos = {x:0,y:0,shoot :false,}

const path = require('path');
"use strict"
app.use(express.static(path.join(__dirname,'public')));

app.get('/',function(req,res){
    res.sendFile(index.html);
});
let localIP;
require('dns').lookup(require('os').hostname(), function (err, add, fam) {
    localIP = add;
    console.log('using this address to connect to the server if you are using the same network:\n'+ add+':'+port);
  })
  
let sockets = [];
io.on('connection', function(socket){
    sockets.push(socket);
    console.log(socket.id + ' has connected to Asteroid-IO number of current player: ' +sockets.length);
    socket.emit('connected', {msg: 'welcome to asteroid-IO'});
    socket.emit('server-addr', {addr: localIP+':'+port})
    socket.on('user-action',function(data){
        broadcast('action-confirm',data, socket);
        broadcast('numberofplayerchange',{
            numberofplayer : sockets.length
        },socket);
    });
    socket.on('disconnect', function(){
        sockets.splice(sockets.indexOf(socket), 1);
        console.log(socket.id + ' has leave remain player: ' +sockets.length);
    })

    socket.on('error', function(){
        sockets.splice(sockets.indexOf(socket), 1);
        console.log(socket.id + ' has leave remain player: ' +sockets.length);
    })
    socket.on('move',function(data){
        if(data.left==true)
        {
            pos.x-=5;
        }
        if(data.right==true)
        {
            pos.x+=5;
        }
        if(data.up==true)
        {
            pos.y-=5;
        }
        if(data.down==true)
        {
            pos.y+=5;
        }
        console.log(pos);
    })
    socket.on('shoot',function(data){
        pos.shoot= data;
    })
    socket.emit('movement', pos);
    
})

function broadcast(header,msg, sender){
    sockets.map(function(socket){
        socket.emit(header,{
            source: sender.id,
            msg: msg
        })
    })
}


let fps = 60
let GameLg = require('./gameLg.js')
let gameLg = new GameLg()
function GameLoop(){
    let startTime = Date.now()
    let duration = 0
    setTimeout(() => {
        gameLg.Update()
        GameLoop()
        duration = 1000/fps - (Date.now() - startTime)
        if(duration<0) duration = 0
    },duration)
}
GameLoop();

server.listen(port, ip, function(){
    console.log('server is running on '+ ip +' '+port);
});